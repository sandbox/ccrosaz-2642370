
-- SUMMARY --
This module show the common social networks to share your content.

It is not using any javascript library, nor the social networks trackers libraries to run...

It is using a template file for the block to display, allowing you to override the output.


-- REQUIREMENTS --

Block module.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --
